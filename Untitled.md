# 读取图片
```python
import os
import matplotlib.pyplot as plt
import cv2
import pandas as pd
import pickle
from keras.preprocessing import image
#····读取图片并灰度化处理显示····#
dataset_path = "G:\Swedish leaf dataset"
CATEGORIES = ["leaf1_Ulmus carpinifolia_", "leaf2_Acer", "leaf3_Salix aurita", "leaf4_Quercus", "leaf5_Alnus incana",
           "leaf6_Betula pubescens", "leaf7_Salix alba 'Sericea'", "leaf8_Populus tremula", "leaf9_Ulmus glabra",
           "leaf10_Sorbus aucuparia", "leaf11_Salix sinerea", "leaf12_Populus", "leaf13_Tilia",
           "leaf14_Sorbus intermedia", "leaf15_Fagus silvatica"]
for category in CATEGORIES:
    path = os.path.join(dataset_path,category)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)
#         img_array = cv2.imread(os.path.join(path,img))
        plt.imshow(img_array, cmap='gray')  # graph it
        plt.show()
        break
    break
print(img_array.shape)
```


    
![png](output_0_0.png)
    


    (2508, 1423)
    


```python
IMG_SIZE=224
new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))

plt.imshow(new_array, cmap='Greens')
plt.show()
```


    
![png](output_1_0.png)
    


# 创建数据集
```python
IMG_SIZE=224
training_data = []
from tqdm import tqdm #读取文件时在输出栏显示进度条
def create_training_data():
    for category in CATEGORIES:

        path = os.path.join(dataset_path,category)
        class_num = CATEGORIES.index(category)

        for img in tqdm(os.listdir(path)):
            try:
#                 img_array = cv2.imread(os.path.join(path,img) )  # 转换为数组
                img_array = cv2.imread(os.path.join(path,img),cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))  # 调整大小以使数据大小规范化
#                 new_array_1 = new_array.copy() #Next three lines only for green_pigment
#                 new_array_1[:,:,0] = 0
#                 new_array_1[:,:,2] = 0
                training_data.append([new_array, class_num])  # 将其添加到我们的training_data中
            except Exception as e:  # in the interest in keeping the output clean...
                pass
            #except OSError as e:
            #    print("OSErrroBad img most likely", e, os.path.join(path,img))
            #except Exception as e:
            #    print("general exception", e, os.path.join(path,img))

create_training_data()

# print(len(training_data))
```

    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:12<00:00,  5.82it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:30<00:00,  2.49it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:13<00:00,  5.64it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:08<00:00,  9.18it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:05<00:00, 14.47it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:11<00:00,  6.44it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:07<00:00, 10.67it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:11<00:00,  6.50it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:16<00:00,  4.58it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:30<00:00,  2.49it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:10<00:00,  7.31it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:15<00:00,  4.96it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:14<00:00,  5.03it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:15<00:00,  4.78it/s]
    100%|██████████████████████████████████████████████████████████████████████████████████| 75/75 [00:10<00:00,  7.07it/s]
    

# 将数据集分成训练集和测试集
```python
import random
import numpy as np
random.shuffle(training_data)
X = []
y = []
for features,label in training_data:
    X.append(features)
    y.append(label)
X = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE,1)
    
```

```python
import tensorflow as tf
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.callbacks import TensorBoard
import time

#将x,y转换成numpy类型
y=np.array(y)
X=np.array(X)
from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test = train_test_split(X,y,test_size=0.2)
```

# 第一次构建模型（效果不理想，数据太少，训练过拟合）
```python
#这个模型有两个卷积层，每个卷积层都有100个卷积核。卷积核的大小均为3 x 3。
# 在每个卷积层后面接一个ReLU激活函数层和一个2 x 2的最大化池化层。
# 池化层的作用是减小特征图的大小，减少参数数量，防止过拟合。
# 接着使用Flatten将3D的特征图转换为1D的特征向量。
# 然后添加一个全连接层，该层有15个神经元，并在其上添加一个softmax激活函数，
# 生成15个输出值，每个值对应一个类别的概率。
model = Sequential()
model.add(Conv2D(100, (3, 3), input_shape=X.shape[1:]))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(100, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))


model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors

model.add(Dense(15))
model.add(Activation('softmax'))

#生成logs文件夹存放训练过程的日志文件（tensorflow环境出问题了，没法实现）
# tensorboard = TensorBoard(log_dir="logs\\{}".format(NAME))

model.compile(loss='sparse_categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

#输入训练数据X和标签y，epochs=10表示训练10轮，validation_split=0.2表示将20%的数据作为验证集进行验证。
history = model.fit(x_train, y_train, epochs=10,validation_data=(x_test,y_test))

```

    Epoch 1/10
    29/29 [==============================] - 13s 187ms/step - loss: 82.8673 - accuracy: 0.2956 - val_loss: 0.8423 - val_accuracy: 0.7244
    Epoch 2/10
    29/29 [==============================] - 4s 153ms/step - loss: 0.5395 - accuracy: 0.8300 - val_loss: 0.6526 - val_accuracy: 0.8089
    Epoch 3/10
    29/29 [==============================] - 4s 150ms/step - loss: 0.1505 - accuracy: 0.9456 - val_loss: 0.7945 - val_accuracy: 0.8000
    Epoch 4/10
    29/29 [==============================] - 4s 145ms/step - loss: 0.0701 - accuracy: 0.9744 - val_loss: 0.8332 - val_accuracy: 0.8489
    Epoch 5/10
    29/29 [==============================] - 4s 145ms/step - loss: 0.0275 - accuracy: 0.9933 - val_loss: 0.6453 - val_accuracy: 0.8622
    Epoch 6/10
    29/29 [==============================] - 4s 145ms/step - loss: 0.0054 - accuracy: 0.9989 - val_loss: 0.8634 - val_accuracy: 0.8756
    Epoch 7/10
    29/29 [==============================] - 4s 145ms/step - loss: 0.0013 - accuracy: 1.0000 - val_loss: 0.8045 - val_accuracy: 0.8800
    Epoch 8/10
    29/29 [==============================] - 4s 145ms/step - loss: 1.6692e-04 - accuracy: 1.0000 - val_loss: 0.7933 - val_accuracy: 0.8889
    Epoch 9/10
    29/29 [==============================] - 4s 146ms/step - loss: 1.1813e-04 - accuracy: 1.0000 - val_loss: 0.8028 - val_accuracy: 0.8978
    Epoch 10/10
    29/29 [==============================] - 4s 149ms/step - loss: 8.6407e-05 - accuracy: 1.0000 - val_loss: 0.8127 - val_accuracy: 0.8978
    


```python
model.summary()
```

    Model: "sequential"
    _________________________________________________________________
     Layer (type)                Output Shape              Param #   
    =================================================================
     conv2d (Conv2D)             (None, 222, 222, 100)     1000      
                                                                     
     activation (Activation)     (None, 222, 222, 100)     0         
                                                                     
     max_pooling2d (MaxPooling2D  (None, 111, 111, 100)    0         
     )                                                               
                                                                     
     conv2d_1 (Conv2D)           (None, 109, 109, 100)     90100     
                                                                     
     activation_1 (Activation)   (None, 109, 109, 100)     0         
                                                                     
     max_pooling2d_1 (MaxPooling  (None, 54, 54, 100)      0         
     2D)                                                             
                                                                     
     flatten (Flatten)           (None, 291600)            0         
                                                                     
     dense (Dense)               (None, 15)                4374015   
                                                                     
     activation_2 (Activation)   (None, 15)                0         
                                                                     
    =================================================================
    Total params: 4,465,115
    Trainable params: 4,465,115
    Non-trainable params: 0
    _________________________________________________________________
    


```python
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']
loss = history.history['loss']
val_loss = history.history['val_loss']
epochs = range(len(acc))
plt.plot(epochs,acc, 'b', label='Training accuracy')
plt.plot(epochs, val_acc, 'r', label='validation accuracy')
plt.title('Training and validation accuracy')
plt.legend(loc='lower right')
plt.figure()

plt.plot(epochs, loss, 'r', label='Training loss')
plt.plot(epochs, val_loss, 'b', label='validation loss')
plt.title('Training and validation loss')
plt.legend()
plt.show()
```


    
![png](output_7_0.png)
    



    
![png](output_7_1.png)
    


# 改进后model
### 进行数据增强
```python
from keras.preprocessing.image import ImageDataGenerator
datagen = ImageDataGenerator(
      rotation_range=20,  # 图像旋转
      shear_range=0.2,  # 剪切
      zoom_range=0.2,  # 缩放
      horizontal_flip=True,  # 水平翻转
)
datagen.fit(x_train)
generated_data = datagen.flow(x_train, y_train, batch_size=32)

```

### 构建模型
```python
model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=X.shape[1:]))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(128, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))


model.add(Conv2D(128, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
model.add(Dense(128))
model.add(Activation('relu'))
model.add(Dense(64))
model.add(Activation('softmax'))

#生成logs文件夹存放训练过程的日志文件（tensorflow环境出问题了，没法实现）
# tensorboard = TensorBoard(log_dir="logs\\{}".format(NAME))

model.compile(loss='sparse_categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

#输入训练数据X和标签y，epochs=10表示训练10轮，validation_split=0.2表示将20%的数据作为验证集进行验证。
history = model.fit(generated_data, epochs=50,validation_data=(x_test,y_test))


```
### 训练

    Epoch 1/50
    29/29 [==============================] - 3s 85ms/step - loss: 4.3830 - accuracy: 0.1644 - val_loss: 1.7105 - val_accuracy: 0.4311
    Epoch 2/50
    29/29 [==============================] - 2s 78ms/step - loss: 1.5380 - accuracy: 0.4533 - val_loss: 0.8895 - val_accuracy: 0.6667
    Epoch 3/50
    29/29 [==============================] - 2s 81ms/step - loss: 1.1314 - accuracy: 0.6233 - val_loss: 0.6525 - val_accuracy: 0.7733
    Epoch 4/50
    29/29 [==============================] - 2s 77ms/step - loss: 0.8908 - accuracy: 0.6811 - val_loss: 0.5505 - val_accuracy: 0.8133
    Epoch 5/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.8611 - accuracy: 0.6889 - val_loss: 0.4788 - val_accuracy: 0.8089
    Epoch 6/50
    29/29 [==============================] - 2s 82ms/step - loss: 0.6180 - accuracy: 0.7867 - val_loss: 0.3086 - val_accuracy: 0.8978
    Epoch 7/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.4578 - accuracy: 0.8333 - val_loss: 0.2222 - val_accuracy: 0.9244
    Epoch 8/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.4701 - accuracy: 0.8511 - val_loss: 0.5856 - val_accuracy: 0.7733
    Epoch 9/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.4856 - accuracy: 0.8422 - val_loss: 0.1706 - val_accuracy: 0.9422
    Epoch 10/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.4570 - accuracy: 0.8400 - val_loss: 0.5481 - val_accuracy: 0.8311
    Epoch 11/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.3741 - accuracy: 0.8667 - val_loss: 0.1624 - val_accuracy: 0.9378
    Epoch 12/50
    29/29 [==============================] - 2s 82ms/step - loss: 0.3623 - accuracy: 0.8733 - val_loss: 0.2120 - val_accuracy: 0.9200
    Epoch 13/50
    29/29 [==============================] - 2s 78ms/step - loss: 0.2585 - accuracy: 0.9033 - val_loss: 0.3312 - val_accuracy: 0.8844
    Epoch 14/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.2534 - accuracy: 0.9167 - val_loss: 0.4305 - val_accuracy: 0.8578
    Epoch 15/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.3276 - accuracy: 0.8778 - val_loss: 0.4584 - val_accuracy: 0.8533
    Epoch 16/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.2830 - accuracy: 0.8911 - val_loss: 0.3428 - val_accuracy: 0.8844
    Epoch 17/50
    29/29 [==============================] - 2s 82ms/step - loss: 0.3680 - accuracy: 0.8600 - val_loss: 0.2914 - val_accuracy: 0.8844
    Epoch 18/50
    29/29 [==============================] - 2s 78ms/step - loss: 0.2078 - accuracy: 0.9278 - val_loss: 0.3698 - val_accuracy: 0.8933
    Epoch 19/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.1781 - accuracy: 0.9322 - val_loss: 0.2645 - val_accuracy: 0.8889
    Epoch 20/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.2052 - accuracy: 0.9267 - val_loss: 0.1321 - val_accuracy: 0.9556
    Epoch 21/50
    29/29 [==============================] - 2s 83ms/step - loss: 0.1729 - accuracy: 0.9433 - val_loss: 0.1993 - val_accuracy: 0.9422
    Epoch 22/50
    29/29 [==============================] - 2s 83ms/step - loss: 0.1456 - accuracy: 0.9444 - val_loss: 0.2035 - val_accuracy: 0.9289
    Epoch 23/50
    29/29 [==============================] - 2s 82ms/step - loss: 0.1234 - accuracy: 0.9556 - val_loss: 0.4267 - val_accuracy: 0.8933
    Epoch 24/50
    29/29 [==============================] - 2s 84ms/step - loss: 0.2661 - accuracy: 0.9133 - val_loss: 0.1252 - val_accuracy: 0.9511
    Epoch 25/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.3133 - accuracy: 0.8822 - val_loss: 0.2806 - val_accuracy: 0.9111
    Epoch 26/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.1471 - accuracy: 0.9444 - val_loss: 0.2445 - val_accuracy: 0.9289
    Epoch 27/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.1558 - accuracy: 0.9433 - val_loss: 0.4494 - val_accuracy: 0.8533
    Epoch 28/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.2011 - accuracy: 0.9378 - val_loss: 0.1414 - val_accuracy: 0.9467
    Epoch 29/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.1824 - accuracy: 0.9333 - val_loss: 0.2054 - val_accuracy: 0.9200
    Epoch 30/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.1106 - accuracy: 0.9600 - val_loss: 0.1882 - val_accuracy: 0.9511
    Epoch 31/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.1133 - accuracy: 0.9544 - val_loss: 0.2032 - val_accuracy: 0.9511
    Epoch 32/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.1324 - accuracy: 0.9522 - val_loss: 0.1032 - val_accuracy: 0.9467
    Epoch 33/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.3362 - accuracy: 0.8733 - val_loss: 0.5771 - val_accuracy: 0.8400
    Epoch 34/50
    29/29 [==============================] - 2s 82ms/step - loss: 0.2409 - accuracy: 0.9089 - val_loss: 0.1221 - val_accuracy: 0.9644
    Epoch 35/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.1160 - accuracy: 0.9600 - val_loss: 0.3829 - val_accuracy: 0.8800
    Epoch 36/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.1392 - accuracy: 0.9589 - val_loss: 0.4001 - val_accuracy: 0.8844
    Epoch 37/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.1175 - accuracy: 0.9556 - val_loss: 0.1767 - val_accuracy: 0.9556
    Epoch 38/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.1708 - accuracy: 0.9411 - val_loss: 0.1781 - val_accuracy: 0.9511
    Epoch 39/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.1120 - accuracy: 0.9544 - val_loss: 0.1408 - val_accuracy: 0.9600
    Epoch 40/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.1018 - accuracy: 0.9644 - val_loss: 0.1782 - val_accuracy: 0.9467
    Epoch 41/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.0857 - accuracy: 0.9656 - val_loss: 0.1023 - val_accuracy: 0.9689
    Epoch 42/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.0687 - accuracy: 0.9767 - val_loss: 0.5150 - val_accuracy: 0.8711
    Epoch 43/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.1106 - accuracy: 0.9578 - val_loss: 0.1332 - val_accuracy: 0.9644
    Epoch 44/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.1058 - accuracy: 0.9522 - val_loss: 0.2262 - val_accuracy: 0.9422
    Epoch 45/50
    29/29 [==============================] - 2s 79ms/step - loss: 0.1035 - accuracy: 0.9656 - val_loss: 0.2113 - val_accuracy: 0.9289
    Epoch 46/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.1080 - accuracy: 0.9622 - val_loss: 0.2219 - val_accuracy: 0.9556
    Epoch 47/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.1140 - accuracy: 0.9556 - val_loss: 0.3011 - val_accuracy: 0.9422
    Epoch 48/50
    29/29 [==============================] - 2s 81ms/step - loss: 0.0706 - accuracy: 0.9767 - val_loss: 0.0671 - val_accuracy: 0.9778
    Epoch 49/50
    29/29 [==============================] - 2s 80ms/step - loss: 0.0898 - accuracy: 0.9700 - val_loss: 0.0959 - val_accuracy: 0.9689
    Epoch 50/50
    29/29 [==============================] - 2s 78ms/step - loss: 0.0671 - accuracy: 0.9767 - val_loss: 0.0788 - val_accuracy: 0.9778
    


```python
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']
loss = history.history['loss']
val_loss = history.history['val_loss']
epochs = range(len(acc))
plt.plot(epochs,acc, 'r', label='Training accuracy')
plt.plot(epochs, val_acc, 'b', label='validation accuracy')
plt.title('Training and validation accuracy')
plt.legend(loc='lower right')
plt.figure()

plt.plot(epochs, loss, 'r', label='Training loss')
plt.plot(epochs, val_loss, 'b', label='validation loss')
plt.title('Training and validation loss')
plt.legend()
plt.show()
```


    
![png](output_10_0.png)
    



    
![png](output_10_1.png)
    



```python

```


```python

```
